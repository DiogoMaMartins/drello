const Cards = require('../models/cards.js');
const mongoose = require('mongoose');


exports.createCards = async (req,res,next) => {
  Cards.find({
    content:req.body.content
  })
  .exec()
  .then(cards => {
    if(cards.length >= 1) {
      return res.status(409).json({
        message:"Card already exists"
      });
    }else{
      const { sectionId, content,labels,userPicture,userId } = req.body;
      const card = new Cards ({
        _id: new mongoose.Types.ObjectId(),
        sectionId,
        content,
        labels,
        userPicture,
        userId
      });

      card
        .save()
        .then(result => {
          res.status(201).json({
            message:'Card created'
          });
        })
        .catch(err => {
          res.status(500).json({
            error:err
          });
        });


    }
  })
  .catch(err => {
      throw err
  })
}


exports.getSections = async (req,res,next) => {
  Cards.find({
    userId:req.body.userId
  })
  .exec()
  .then(cards => {
    return res.status(200).json({
      cards
    })  
  })
  .catch(err => {
    console.log("im err:",err)
  })
}

const Section = require('../models/section.js');
const mongoose = require('mongoose');


exports.createSection = async (req,res,next) => {
  Section.find({
    title:req.body.title
  })
  .exec()
  .then(section => {
    if(section.length >= 1) {
      return res.status(409).json({
        message:"Section already exists"
      });
    }else{
      const { userId, title,creatable,done,cards } = req.body;
      const section = new Section ({
        _id: new mongoose.Types.ObjectId(),
        userId,
        title,
        creatable,
        done,
        cards
      });

      section
        .save()
        .then(result => {
          res.status(201).json({
            message:'Section created'
          });
        })
        .catch(err => {
          res.status(500).json({
            error:err
          });
        });


    }
  })
  .catch(err => {
      throw err
  })
}


exports.getSections = async (req,res,next) => {
  Section.find({
    userId:req.body.userId
  })
  .exec()
  .then(section => {
    return res.status(200).json({
      section
    })
  })
  .catch(err => {
    console.log("im err:",err)
  })
}

const User = require('../models/user.js');
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const checkAuth = require('../middleware/')
const config = require('../middleware/config.js');
const crypto = require('crypto');
const mailer = require('../../modules/mailer');
const multer = require('multer');
const upload = require('../handlers/multer');

exports.signup = async (req,res,next) => {
    User.find({
      username:req.body.username
    })
    .exec()
    .then(user => {
      if (user.length >= 1) {
        return res.status(409).json({
          message: "Username already exists"
        });
      } else {
        bcrypt.hash(req.body.password, 10 , (err,hash) => {
          if(err) {
            return res.status(500).json({
              error:err
            });
          } else {
            const user = new User ({
              _id: new mongoose.Types.ObjectId(),
              username: req.body.username,
              firstname:req.body.firstname,
              lastname:req.body.lastname,    
              email:req.body.email,
              password:hash,
              image: req.file.path,
              ip:req.headers["X-Forwarded-For"] || req.connection.remoteAddress,
            });
            user
              .save()
              .then(result => {
                res.status(201).json({
                  message:'Welcome to Drello'
                });
              })
              .catch(err => {
                res.status(500).json({
                  error:err
                });
              });
          }
        })
      }
    })
    .catch(err => {
      throw err
    })

};

exports.login = (req,res,next) => {
    User.find({
      username:req.body.username
    })
    .exec()
    .then(user => {
      if (user.length < 1) {
        return res.status(401).json({
          message: "Auth failed user doesn't exists"
        });
      }

      bcrypt.compare(req.body.password, user[0].password, (err, result) => {
        if (err) {
          return res.status(401).json({
            message: 'Auth failed password'
          });
        }

        if (result) {
          const token = jwt.sign({
            username:user[0].username,
            userId:user[0]._id

          },
          config.secret, {
              expiresIn: "16h"
          }
        );

        return res.status(200).json({
          success:true,
          id: user[0]._id,
          message: 'Auth successfull',
          token:token
        });
        }
        res.status(401).json({
          message: 'Auth failed'
        });
      });

    })
    .catch(err => {
      res.status(500).json({
        error:err
      });
    });
};

exports.forgot_password = async (req, res) => {
  const { userName, email } = req.body;

	try {
		const user = await User.findOne({ userName });
		const userEmail = await user.email
	if(email === userEmail && user.userName === userName ){	const token = crypto.randomBytes(20).toString('hex');

		const now = new Date();

		now.setHours(now.getHours() + 1);

		await User.findByIdAndUpdate(user.id, {
			'$set': {
				passwordResetToken: token,
				passwordResetExpires: now,
			}
		});

		mailer.sendMail({
			to:email,
			from: 'hvhnapp@gmail.com',
			subject: 'Reset Password',
			template: 'auth/forgot_password',
			context: { token },
		}, (err) => {
			if (err)
				return res.status(400).send({ error: 'Cannot send forgot password email'})

				return res.send()
		})
}else{
		return res.status(400).send({ error: "User or Email not found "});
}

  }catch(err){
    res.status(400).send({ error: 'Erro on forgot password, try again'});
  }
};

exports.reset_password =  async (req, res) => {
	const { userName, email, token, password } = req.body;

		try {
			const user = await User.findOne({ userName })
				.select('+passwordResetToken passwordResetExpires');

				if (!user)
					return res.status(400).send({ error: 'User not found' });

				if (token !== user.passwordResetToken)
					return res.status(400).send({ error: 'Token invalid' });

				const now = new Date();

				if (now > user.passwordResetExpires)
						return res.status(400).send({error: 'Token expired, generate a new one '})

				const hash = await bcrypt.hash(password,10)
				user.passwordResetExpires = ""
				user.passwordResetToken = "",
				user.password = hash;

				await user.save();

				res.send();

		} catch (err) {
			console.log(err)
			res.status(400).send({ error: 'Cannot reset password, try again '});

		}
};

exports.deleteUser = async(req,res,next) => {
  let username = req.params.username;
  try{
    const user = await MissionsInGame.findOneAndRemove({ username }, function(error,usern) {
    console.log(usern)
    })
  }catch(err){
    return err
  }


};

exports.getIpaddress = (req,res,next) => {
  var getClientIp = function(req) {
    return (req.headers["X-Forwarded-For"] ||
            req.headers["x-forwarded-for"] ||
            '').split(',')[0] ||
           req.client.remoteAddress;
};
var ip = req.headers["X-Forwarded-For"] || req.connection.remoteAddress;
res.send(ip)
console.log("im client ip",ip)
};

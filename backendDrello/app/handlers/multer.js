const multer = require('multer');

module.exports = multer ({
  storage: multer.diskStorage({
    destination: function(req,file,cb) {
      cb(null, './uploads')
    },
    filename: function(req,file,cb) {
      cb(null, new Date().toISOString() + file.originalname);
    }
  }),
  fileFilter:(req,file,cb) => {
    if(file.mimetype.match(file.mimetype === 'image/jpeg' || file.mimetype === 'image/png' || file.mimetype === 'image/pjpeg')){
      cb(new Error('File is not supported'), false)
      return
    }

    cb(null,true)
  }

})


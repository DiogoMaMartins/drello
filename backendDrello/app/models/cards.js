const mongoose = require('mongoose');

const cardSchema = mongoose.Schema({
  id:mongoose.Schema.Types.ObjectId,
  userId:{
    type:String,
    required:true,
    unique:false,
  },
  sectionId:{
    type:String,
    required:true,
    unique:false,
  },
  content:{
    type:String,
    required:true,
    unique:true,
  },
  labels:[],
  userPicture:{
    type:String,
    required:true,
    unique:false,
  }
})

module.exports = mongoose.model('Cards',cardSchema);

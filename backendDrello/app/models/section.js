const mongoose = require('mongoose');

const sectionSchema = mongoose.Schema({
  _id:mongoose.Schema.Types.ObjectId,
  userId:{
    type:String,
    required:true,
  },
  title:{
    type:String,
    required:true,
    unique:true
  },
  creatable:{
    type:Boolean,
    required:true,
    unique:true
  },
  done:{
    type:Boolean,
    required:false,
    unique:true
  },
  cards:[]
});

module.exports = mongoose.model('Section',sectionSchema);

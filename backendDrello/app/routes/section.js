const express = require("express");
const router = express.Router();
const mongoose = require('mongoose');
const SectionController = require('../controllers/section');


router.post('/createSection',SectionController.createSection)
router.get('/getSections',SectionController.getSections)
module.exports = router;

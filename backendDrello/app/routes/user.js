const express = require("express");
const router = express.Router();
const mongoose = require('mongoose');
const checkAuth = require('../middleware/')
const upload = require('../handlers/multer');
const UserController = require('../controllers/user');

router.post('/signup',upload.single("picture"), UserController.signup);
router.post('/login',UserController.login);
router.post('/forgot_password',UserController.forgot_password);
router.post('/reset_password',UserController.reset_password);
router.post('/deleteUser/:username',UserController.deleteUser);
router.get('/ip',UserController.getIpaddress)
module.exports = router;

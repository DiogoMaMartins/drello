const express = require('express');
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');
const PORT = process.env.PORT || 4000;
app.use(cors());
let config = require('./config');

mongoose.connect(config.database, { useNewUrlParser: true });
mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
app.set('superNode-auth', config.configName);


const userRoutes = require('./app/routes/user');
const sectionRoutes = require('./app/routes/section');

app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

app.use('/',userRoutes,sectionRoutes);



app.listen(PORT)
console.log("Application running in port: ",PORT)

const path = require('path');
const nodemailer = require('nodemailer');
const hbs = require('nodemailer-express-handlebars');

/*const transport = nodemailer.createTransport({
  host: "smtp.mailtrap.io",
  port: 2525,
  auth: {
  user: "da6ff5ad4ac447",
  pass: "a6f95442bf2a49"
}
})*/


const transport = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'hvhnapp@gmail.com',
    pass: 'MijnMobileApp'
  }
})



transport.use('compile', hbs ({
  viewEngine: {
      extName: '.hbs',
      partialsDir: path.resolve('./app/resources'),
      layoutsDir: path.resolve('./app/resources'),
    },
  viewPath: path.resolve('./app/resources/mail/'),
  extName: '.html',
}))

module.exports = transport;


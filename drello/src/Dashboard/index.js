import React from 'react';
import { DndProvider } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';

import GlobalStyle from '../styles/global';
import HeaderDashboard from '../components/HeaderDashboard';
import Board from '../components/Board';



function Dashboard() {
  return (
    <DndProvider backend={HTML5Backend}>
      <HeaderDashboard/>
      <Board/>
      <GlobalStyle/>

    </DndProvider>
  );
}

/*function Dashboard() {
  return (
      <CreateFolder/>

  );
}
*/
export default Dashboard;

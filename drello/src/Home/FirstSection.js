import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
const useStyles = makeStyles(theme => ({
  root: {
    height: '100vh',
    background:'#7159c1',
    marginTop:20,
  },
  info:{
    color:'#FFF',
  },
  img:{
    height: 200,
    width:200,
    marginTop:'20px',
  }

  }));

  export default function FirstSection() {
    const classes = useStyles();

    return (
      <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <Grid item xs={12} sm={6} md={6} style={{display:'flex',flexDirection:'column',alignItems:'center',justifyContent:'center  '}}>
      <Typography component="h1" variant="h1" className={classes.info} justify='center'  align="center">
        What is  Drello?
      </Typography>

      <Typography component="h2"  className={classes.info} justify='center'  align="center">
        Drello is task management like trello.I create Drello just to have somme apps to show to my clients.
      </Typography>

      </Grid>
      <Grid item xs={12} sm={6} md={6} style={{display:'flex',alignItems:'center',justifyContent:'center  '}}>
      <img  src="https://wonderwheelstore.com/wp-content/uploads/2017/11/feature-todo-list-min.png" alt="https://apprecs.org/gp/images/app-icons/300/8a/io.tinbits.memorigi.jpg"/>
      </Grid>

      </Grid>
    )
  }

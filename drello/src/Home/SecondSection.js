import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
const useStyles = makeStyles(theme => ({
  root: {
    height: '50vh',
  },
  img:{
    height: 150,
    width:150,
    marginTop:'20px',
  }

  }));

  export default function SecondSection() {
    const classes = useStyles();

    return (
      <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <Grid item xs={12}>
        <Typography  component="h3"  variant="h3"  justify='center'  align="center">
          Why i need to use Drello?
        </Typography>
      </Grid>
      <Grid container>
        <Grid item xs={12} sm={3} md={3} style={{display:'flex',flexDirection:'column',alignItems:'center',justifyContent:'center  '}}>
        <img className={classes.img} src="https://camo.envatousercontent.com/2c9fab3611c27f9064046cd00ec3f28cf455e250/687474703a2f2f6661737464656c6976657279736572766963652e63612f77702d636f6e74656e742f75706c6f6164732f323031342f30352f666173742d64656c69766572792e706e67" alt=""/>
        <Typography  component="h3" variant="h3"   justify='center'  align="center">
          Is Fast
        </Typography>
        </Grid>

        <Grid item xs={12} sm={3} md={3} style={{display:'flex',flexDirection:'column',alignItems:'center',justifyContent:'center  '}}>
        <img className={classes.img} src="https://www.newradio.it/wpnr/wp-content/uploads/2017/01/free-tag.png" alt=""/>
        <Typography  component="h3" variant="h3"   justify='center'  align="center">
          Free
        </Typography>
        </Grid>

        <Grid item xs={12} sm={3} md={3} style={{display:'flex',flexDirection:'column',alignItems:'center',justifyContent:'center  '}}>
        <img className={classes.img} src="https://wonderwheelstore.com/wp-content/uploads/2017/11/feature-todo-list-min.png" alt=""/>
        <Typography  component="h3" variant="h3"   justify='center'  align="center">
          Control
        </Typography>
        </Grid>

        <Grid item xs={12} sm={3} md={3} style={{display:'flex',flexDirection:'column',alignItems:'center',justifyContent:'center  '}}>
        <img className={classes.img} src="https://www.elenabeser.com/wp-content/uploads/2015/06/lock@2x.png" alt=""/>
        <Typography  component="h3" variant="h3"   justify='center'  align="center">
          Safe
        </Typography>
        </Grid>

      </Grid>

      </Grid>
    )
  }

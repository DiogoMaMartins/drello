import React, { Component } from 'react';

//import { Redirect } from 'react-router-dom';
import Header from '../components/Header';
import FirstSection from './FirstSection';
import SecondSection from './SecondSection';

class Home extends Component {
  render() {
    return (
      <div>
      <Header/>
      <FirstSection/>
      <SecondSection/>

      </div>
    );
  }
}

export default Home;

import React, { Component } from 'react';

import { BrowserRouter,Route,Switch,Redirect } from 'react-router-dom';

import { isAuthenticated } from './auth';

//Pages <PrivateRoute exact path="/dashboard" component={Dashboard}/>

import Home from '../Home';
import SignIn from '../SignIn';
import SignUp from '../SignUp';
import Dashboard from '../Dashboard';
import { Provider } from 'react-redux';
import store from '../store';
import GlobalStyle from '../styles/global';

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
  {...rest}
    render={props =>
    isAuthenticated() ? (
      <Component {...props}/>
      ): (
      <Redirect to={{ pathname: '/', state: { from: props.location }}}/>
    )
    }
    />
  );

class Routes extends Component {
    render() {
      return (
         <Provider store={store}>
              <BrowserRouter>
              <GlobalStyle/>
                <Switch>
                  <Route exact path='/' component={Home} />
                  <Route exact path='/signIn' component={SignIn} />
                  <Route exact path='/signup' component={SignUp} />
                  <PrivateRoute exact path="/dashboard" component={Dashboard}/>


                </Switch>
              </BrowserRouter>
            </Provider>
      )
    }
}

export default Routes;


/*
import React from 'react'
import PropTypes from 'prop-types'
import { Provider } from 'react-redux'

import { BrowserRouter,Route,Switch,Redirect } from 'react-router-dom';

import { isAuthenticated } from './auth';


//Pages

import Home from '../Home';
import SignIn from '../SignIn';
import SignUp from '../SignUp';
import Dashboard from '../Dashboard';
import store from '../store';
const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
  {...rest}
    render={props =>
    isAuthenticated() ? (
      <Component {...props}/>
      ): (
      <Redirect to={{ pathname: '/', state: { from: props.location }}}/>
    )
    }
    />
  );

const Routes  = ({ store }) => (
          <Provider store={store}>
            <BrowserRouter>
              <Switch>
                <Route exact path='/' component={Home} />
                <Route exact path='/signIn' component={SignIn} />
                <Route exact path='/signup' component={SignUp} />
                <PrivateRoute exact path="/dashboard" component={Dashboard}/>
              </Switch>
            </BrowserRouter>
        </Provider>
      )



Routes.propTypes = {
  store: PropTypes.object.isRequired
}

export default Routes;
*/

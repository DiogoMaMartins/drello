/*import React from 'react';
import Cards from '../components/Cards';
import { MdLock } from 'react-icons/md';

export default function SignIn() {
  const forms = [
    {
      type:'text',
      placeholder:'Username'
    },
    {
      type:'password',
      placeholder:'Password'
    }
  ]
  return (
    <div style={{display:'flex',background:'#7159c1',alignItems:'center',justifyContent:'center',height:'100vh'}}>
      <Cards title='Login to use Drello' info='Enter your username and password' btnText='SignIn' form={forms} icon={<MdLock size={40} color="#7159c1"/>
}/>
    </div>
  );
}
*/

import React, { Component } from 'react';
import LoginForm from '../components/LoginForm';
import { MdLock } from 'react-icons/md';

export default class SignIn extends Component {

  render(){
    return (
      <div style={{display:'flex',background:'#7159c1',alignItems:'center',justifyContent:'center',height:'100vh'}}>
        <LoginForm title='Login to use Drello' info='Enter your username and password' btnText='SignIn'  props={this.props.history} icon={<MdLock size={40} color="#7159c1"/>}/>
      </div>
    );
  }

}

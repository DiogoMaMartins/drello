import React from 'react';
import SignUpForm from '../components/SignUpForm';


export default function SignUp() {
  
  return (
    <div style={{display:'flex',background:'#7159c1',alignItems:'center',justifyContent:'center',height:'100vh'}}>
      <SignUpForm />

    </div>
  );
}

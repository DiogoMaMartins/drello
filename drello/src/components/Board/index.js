import React, { useState } from 'react';
import produce from 'immer';
import { MdAdd } from 'react-icons/md';
import { loadLists } from '../services/api';

import BoardContext from './context';

import List from '../List';

import { Container } from './styles';

const data = loadLists();

export default function Board() {
  const [lists, setLists] = useState(data);

  function move(fromList, toList, fromPosition, toPosition) {
    console.log("eu vou da lista :",fromList,"para a lista ",toList,"parto da posicao:",fromPosition,"para a posicao :",toPosition)
    setLists(produce(lists, draft => {
      const dragged = draft[fromList].cards[fromPosition];

      draft[fromList].cards.splice(fromPosition, 1);
      draft[toList].cards.splice(toPosition, 0, dragged);
    }))
  }

  return (
    <BoardContext.Provider value={{ lists, move }}>
      <Container>
        <MdAdd size={40} color="#7159c1" style={{position:'absolute',right:100,top:100}}/>
        {lists.map((list, index) => <List key={list.title} index={index} data={list} />)}
      </Container>
    </BoardContext.Provider>
  );
}

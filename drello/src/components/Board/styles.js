import styled from 'styled-components';

export const Container = styled.div`
  display:flex;
  padding:30px 0;
  margin-top:100px;
  height:calc(100% - 80%);
`;

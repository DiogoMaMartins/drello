import React from 'react';
import { Container,Button } from './styles';
import Input from '../Input';

export default function Cards ({title,info,btnText,form,icon}) {
  return (
    <Container>
      <header>
        <div style={{padding:10,display:'flex',flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
         <h1 style={{color:'#c4c4c4'}}>{title}</h1>
         {icon}
        </div>
        <p  style={{paddingLeft:10,color:'#c4c4c4'}}>{info}</p>
      </header>
      <div style={{marginTop:120}}>
      {form.map(input => (
        <Input type={input.type}  placeholders={input.placeholder} />
      ))}
      <Button><h3>{btnText}</h3></Button>
      </div>
    </Container>
  )
}

import React, { useState }  from 'react';
import { Container,Button,Input } from './styles';
import axios from 'axios';

export default function LoginForm ({title,info,btnText,props,icon}) {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');


  let login = (e) => {
    axios.post('http://localhost:4000/user/login',{
      username,
      password
    })
    .then(response => {
      console.log("im id",response.data.id)
      sessionStorage.setItem('token', response.data.token);
      sessionStorage.setItem('id', response.data.id);
    })
    .then(redirect => {
      props.push('/dashboard')
    })
    .catch(err => console.log('im error:',err))
    console.log("im password",password,'im username',username)
  }
  return (
    <Container>
      <header>
        <div style={{padding:10,display:'flex',flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
         <h1 style={{color:'#c4c4c4'}}>{title}</h1>
         {icon}
        </div>
        <p  style={{paddingLeft:10,color:'#c4c4c4'}}>{info}</p>
      </header>
      <div style={{marginTop:120}}>
      <Input type='text' placeholder='Username...'  value={username} onChange={(e) =>setUsername(e.target.value)}/>
      <Input type='password' placeholder='Password...' value={password} onChange={(e) =>setPassword(e.target.value)}/>

      <Button onClick={login}><h3>{btnText}</h3></Button>
      </div>
    </Container>
  )
}

import React from 'react';

import { Container,Actions } from './styles';
import { Link } from 'react-router-dom'
export default function Header() {
  return (
    <Container>
      <h1>Drello</h1>
        <Actions>
         <Link style={{color:'#FFF',fontSize:24,textDecoration:'none',marginLeft:10,fontWeight:500}} to="/signIn">Sign In</Link>
          <Link style={{color:'#FFF',fontSize:24,textDecoration:'none',marginLeft:10,fontWeight:500}} to="/signUp">Sign Up</Link>
        </Actions>
    </Container>
  )
}

import styled from 'styled-components';

export const Container = styled.div`
  top:0;
  left:0;
  right:0;
  position:absolute;
  height:80px;
  padding: 0 30px;
  background:#7159c1;
  justify-content:space-between;
  color:#FFF;
  display:flex;
  align-items:center;
  border-bottom:2px solid #FFF;
`;

export const Actions = styled.div`
 display:flex;
 flex-direction:row;
 align-items:center;
`;

export const Button = styled.button`
  margin-right:10px;
  color:#FFF;
  background:#7159c1;
  border:none;
  cursor:pointer;
`;

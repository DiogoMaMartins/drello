import React from 'react';
import { Container,Actions,Image } from './styles';
export default function HeaderDashboard() {
  return (
    <Container>
      <h1>Drello</h1>
        <Actions>
          <Image src="https://api.adorable.io/avatars/285/abott@adorable.png"  alt="userPicture"/>
        </Actions>
    </Container>
  )
}

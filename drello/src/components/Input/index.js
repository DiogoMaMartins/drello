import React from 'react';

import { Container } from './styles';

export default function Input({placeholders}) {
  return (
    <Container placeholder={placeholders}/>
  )
}

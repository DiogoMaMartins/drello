import styled from 'styled-components';

export const Container = styled.input`
  height:50px;
  width:95%;
  font-size:18px;
  padding-left:10px;
  margin-top:10px;
  margin-bottom:10px;
  border:2px solid #c9c9c9;
  color:#000;
`;

import React from 'react';

import { MdAdd } from 'react-icons/md';

import Card from '../Card';

import { Container } from './styles';

const test = {
          id: 1,
          content: 'DIogo',
          labels: ['#7159c1'],
          user: 'https://api.adorable.io/avatars/73/abott@adorable.png'
  }

export default function List({ data, index: listIndex }) {
  return (
    <Container done={data.done}>
      <header>
        <h2>{data.title}</h2>
        {data.creatable && (
          <button type="button">
            <MdAdd size={24} color="#FFF" />
          </button>
        )}
      </header>

      <ul>
      
        { data.cards.map((card, index) => (


          <Card
            key={card.id}
            listIndex={listIndex}
            index={index}
            data={card}
          />
        )) }
      </ul>
    </Container>
  );
}

import styled, { css } from 'styled-components';

export const Container = styled.div`
  display:flex;
  flex-direction:column;
  justify-content:space-around;
  position:relative;
  background:#c4c4c4;
  border-radius:5px;
  margin-bottom:10px;
  padding:15px;
  box-shadow:0 1px 4px 0 rgba(192,208,230,0.8);
  cursor:grab;
  min-height:200px;
  width:500px;

  header {
    position:absolute;
    top:0;
    left:0;
    right:0;
    min-height:100px;
    background-color:#FFF;
  }

`;

export const Button = styled.button`
  height:40px;
  width:96%;
  color:#FFF;
  background:#7159c1;
  border-radius:2px;
  margin-top:10px;
  cursor:pointer;
  border:1px  solid #7159c1;
`;


export const Input = styled.input`
  height:50px;
  width:95%;
  font-size:18px;
  padding-left:10px;
  margin-top:10px;
  margin-bottom:10px;
  border:2px solid #c9c9c9;
  color:#000;
`;

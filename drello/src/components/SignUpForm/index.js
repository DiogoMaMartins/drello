import React, {useState} from 'react';
import { Container,Button,Input } from './styles';
import { MdEdit } from 'react-icons/md';
import axios from 'axios';

export default function SignUpForm () {
  const [picture, setPicture] = useState(null);
  const [ username, setUsername] = useState('');
  const [ firstName, setFirstname] = useState('');
  const [ lastName, setLastname ] = useState('');
  const [email,setEmail] = useState('');
  const [password,setPassword] = useState('');

  let signUp = () => {
    console.log("image",picture,"im username:",username,"im lastname:",lastName,"fisrtname",firstName,"Email",email,"password",password)
    
    let body = new FormData();
    body.set('username',username);
      body.set('firstname',firstName);
      body.set('lastname',lastName);
      body.set('email',email);
      body.set('password',password);
      body.set('picture',picture);

    axios({
    method: 'post',
    url: 'http://localhost:4000/signup',
    data: body,
    config: { headers: {'Content-Type': 'multipart/form-data' }}
    })
    .then(function (response) {
        //handle success
        console.log(response);
    })
    .catch(function (response) {
        //handle error
        console.log(response);
    });
  }


  return (
    <Container>
      <header>
        <div style={{padding:10,display:'flex',flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
         <h1 style={{color:'#c4c4c4'}}>Sign Up</h1>
         <MdEdit size={40} color="#7159c1"/>
        </div>
        <p  style={{paddingLeft:10,color:'#c4c4c4'}}>Fill this form to have access</p>
      </header>
      <div style={{marginTop:120}}>
       <Input type="file" placeholder="Pick one picture.."   onChange={(e) => setPicture(e.target.files[0])}/>
       <Input type='text' placeholder='Username...'  value={username} onChange={(e) =>setUsername(e.target.value)}/>
       <Input type='text' placeholder='FirstName...'  value={firstName} onChange={(e) =>setFirstname(e.target.value)}/>
       <Input type='text' placeholder='LastName...' value={lastName} onChange={(e) =>setLastname(e.target.value)}/>
       <Input type='text' placeholder='Email Address...' value={email} onChange={(e) =>setEmail(e.target.value)}/>

      <Input type='password' placeholder='Password...' value={password} onChange={(e) =>setPassword(e.target.value)}/>

    
      <Button onClick={signUp}><h3>Sign Up</h3></Button>
      </div>
    </Container>
  )
}

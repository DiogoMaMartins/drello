import { combineReducers } from 'redux';

import todos from './todos';
import authentication from './authentication';
export default combineReducers({
	todos,
	authentication
})
